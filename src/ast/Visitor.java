package ast;

public interface Visitor {

	 public Object visit (Node n); 

	    /* Visit method for declarations and statements */
	    public Object visit (StmtNode n); 
	    public Object visit (DeclNode n); 
	    public Object visit (List n); 


	    /* Visit method for expr nodes */
	    public Object visit (ExprNode n); 
	    public Object visit (AddNode n); 
	    public Object visit (MultiplyNode n); 
	    public Object visit (NumNode n);
	    public Object visit (IdNode n);
	    public Object visit (SubtractionNode n);
	    public Object visit (DivisionNode n);
	    
	    
}
