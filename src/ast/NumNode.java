package ast;

public abstract class NumNode extends ExprNode{
	
	public Object accept(Visitor v){
		return v.visit(this);
	}

}
