package ast;

public class DivisionNode extends ExprNode {
	
	public DivisionNode(ExprNode left, ExprNode right)
	{
		this.left = left;
		this.right= right;
	}

	 public Object accept(Visitor v) {
			return v.visit(this);
		    }
	 

}
