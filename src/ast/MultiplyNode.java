package ast;

public class MultiplyNode extends ExprNode{

	
	public MultiplyNode(ExprNode left, ExprNode right)
	{
		this.left = left;
		this.right= right;
	}

	 public Object accept(Visitor v) {
			return v.visit(this);
		    }
}
