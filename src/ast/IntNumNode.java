package ast;

public class IntNumNode extends NumNode{
	
	public Integer value;
	
	  public IntNumNode(Integer i) {
			this.left=null;
			this.right=null;
			value = i;
		    }
}
