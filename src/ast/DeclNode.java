package ast;

public class DeclNode extends Node{
	
	public String type;
	public String id;
	
	public DeclNode(String type, String id)
	{
		this.type = type;
		this.id = id;
	}
	
	public Object accept(Visitor v) {
		return v.visit(this);
	    }

}
